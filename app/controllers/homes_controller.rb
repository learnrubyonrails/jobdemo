class HomesController < ApplicationController
  def index
    @user = User.all
  end

  def show
    @user =  User.find(params[:id]) if params[:id].present?
    return unless @user
    @payment_history = [
      {status: "canceled" ,data: @user.user_payments.canceled},
      {status: "paid" ,data: @user.user_payments.paid},
      {status: "unpaid" ,data: @user.user_payments.unpaid},
      {status: "missing" ,data: @user.user_payments.missing}
    ]

  end
end