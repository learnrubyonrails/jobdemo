module HomeHelper
  def get_paid_count(single_data)
    single_data.user_payments.paid_count
  end

  def get_unpaid_count(single_data)
    single_data.user_payments.un_paid_count
  end

  def get_canceled_count(single_data)
    single_data.user_payments.canceled_count
  end

  def get_missing_count(single_data)
    single_data.user_payments.missing_count
  end
end
