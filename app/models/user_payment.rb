class UserPayment < ApplicationRecord
  belongs_to :user
  enum status:  { unpaid: "unpaid", missing: "missing" ,paid: "paid", canceled: "canceled"}

  scope :paid_count,  -> { where(status: "unpaid").count}
  scope :missing_count,  -> { where(status: "missing").count}
  scope :un_paid_count,  -> { where(status: "unpaid").count}
  scope :canceled_count,  -> { where(status: "canceled").count}

end
