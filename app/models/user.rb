class User < ApplicationRecord
  validates :email,:name , presence: true
  validates  :email , uniqueness: true
  has_many :user_payments
end
