require 'faker'

puts "Data has been created"
1.upto 10 do
  ActiveRecord::Base.transaction do
    user = User.create(name: Faker::Name.name,email: Faker::Internet.email)
    1.upto 5 do
      user.user_payments.create(order_date:Faker::Date.in_date_period  ,received_date:Faker::Date.in_date_period  ,status: "paid")
      user.user_payments.create(order_date:Faker::Date.in_date_period  ,received_date:Faker::Date.in_date_period  ,status: "unpaid")
      user.user_payments.create(order_date:Faker::Date.in_date_period  ,received_date:Faker::Date.in_date_period  ,status: "canceled")
      user.user_payments.create(order_date:Faker::Date.in_date_period  ,received_date:Faker::Date.in_date_period  ,status: "missing")
    end
  end
end

1.upto 3 do
  ActiveRecord::Base.transaction do
    1.upto 2 do |index|
      user = User.find(index)
      user.user_payments.create(order_date:Faker::Date.in_date_period  ,received_date:Faker::Date.in_date_period  ,status: "canceled")
      user.user_payments.create(order_date:Faker::Date.in_date_period  ,received_date:Faker::Date.in_date_period  ,status: "missing")
    end
  end
end
