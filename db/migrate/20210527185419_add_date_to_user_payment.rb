class AddDateToUserPayment < ActiveRecord::Migration[6.1]
  def change
    add_column :user_payments, :order_date, :date
    add_column :user_payments, :received_date, :date
  end
end
