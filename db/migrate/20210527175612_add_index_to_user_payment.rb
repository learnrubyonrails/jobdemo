class AddIndexToUserPayment < ActiveRecord::Migration[6.1]
  def change
    add_index :user_payments , :status
  end
end
