class CreateUserPayments < ActiveRecord::Migration[6.1]
  def change
    create_table :user_payments do |t|
      t.string :status
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
